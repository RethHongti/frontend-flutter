import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {

  List <Map<String,dynamic>> menu = [
    {
      "icon" : Icons.widgets,
      "title" : "Stock"
    },
    {
      "icon" : Icons.edit,
      "title" : "Update Stock"
    },
    {
      "icon" : Icons.assessment,
      "title" : "Reports"
    },
    {
      "icon" : Icons.person,
      "title" : "Employees"
    },
    {
      "icon" : Icons.phone_android,
      "title" : "Using"
    },
    {
      "icon" : Icons.report,
      "title" : "About"
    },
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text("FanJo Mall",style: TextStyle(fontSize: 18),),
        backgroundColor: Color.fromRGBO(5, 81, 148, 1),
        actions: [
          IconButton(icon: Icon(Icons.logout), onPressed: (){}),
        ],
      ),
      body: _buildBody,
    );
  }

  get _buildBody{
    return Column(
      children: [
        Container(
          width: double.infinity,
          height: 70,
          color: Colors.white,
          child: Image.asset("images/Logo.png")
        ),
        Text("Exchange Rate 1\$ = 4200៛",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),),
        Container(
          //padding: EdgeInsets.only(right: 10,left: 10),
          height: MediaQuery.of(context).size.height * 0.75,
          child: GridView.builder(
            physics: NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                mainAxisSpacing: 0,
                crossAxisSpacing: 0
              ),
              itemCount: 6,
              itemBuilder: (context, index) {
                return Container(
                  decoration: BoxDecoration(
                    //borderRadius: BorderRadius.all(Radius.circular(5)),
                    border: Border(left: BorderSide(color: Colors.black26),bottom: BorderSide(color: Colors.black26)),
                  ),
                  child: gridMenu(menu[index]['icon'],menu[index]['title']),
                );
              },
          ),
        ),
      ],
    );
  }

   gridMenu(icon,title) {
    return InkWell(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(icon,size: 40,color: Color.fromRGBO(5, 81, 148, 1),),
          Text(title,style: TextStyle(fontSize: 12),),
        ],
      ),
      onTap: (){
        print(title);
      },
    );
  }
}
